import Core.*;
import Core.System;
import Core.Exceptions.NoEntityException;
import Core.Managers.*;
import util.structures.*;

import static java.lang.System.out;
import static java.lang.System.nanoTime;
public class Main {


	public static void main(String[] args){
		/*
		ArrayList<Integer> a = new ArrayList<Integer>(20);
		
		out.println("Size: "+a.size());
		out.println(a);
		
		for(int i=0; i<30; i++)
			a.add(i);
		out.println(a);
		
		a.add(-1, 10);
		out.println(a);
		
		for(int i=0; i<20; i++)
			a.remove();
		out.println(a);
		a.remove(5);
		out.println(a);
		*/
		
		for(int i=0; i<1000000000L; ){
			i++;
		}
		
		
		Realm realm = new Realm(10,7);
		out.println(realm.getPriority());
		out.println("Created realm");
		Entity e1 = new Entity(realm);
		Entity e2 = new Entity(realm);
		Entity e3 = new Entity(realm);
		out.println("Create entities");
		TestComponent c1 = new TestComponent(realm);
		TestComponent c2 = new TestComponent(realm);
		TestComponent c3 = new TestComponent(realm);
		TestComponent c4 = new TestComponent(realm);
		TestComponent c5 = new TestComponent(realm);
		TestComponent c6 = new TestComponent(realm);
		testPosition p1 = new testPosition(1,2,3,realm);
		testPosition p2 = new testPosition(1,2,3,realm);
		testPosition p3 = new testPosition(1,2,3,realm);
		testPosition p4 = new testPosition(1,2,3,realm);
		testPosition p5 = new testPosition(1,2,3,realm);
		testPosition p6 = new testPosition(1,2,3,realm);
		out.println("Create component");
		TestSystem s1 = new TestSystem(realm, true);
		out.println("Create System");
		realm.add(e1);
		realm.add(e2);
		realm.add(e3);
		out.println("Add entity");
		realm.add(s1);
		out.println("Add System");
		
		
		
		realm.addCommand("Pause");
		out.println("Paused");
		long end, start = nanoTime();
		realm.add(c3, e3);
		realm.add(c4, e2);
		realm.add(c5, e3);
		realm.add(c1, e1);
		realm.add(c6, e2);
		realm.add(c2, e1);
		
		realm.add(p1, e1);
		realm.add(p3, e3);
		realm.add(p4, e2);
		realm.add(p5, e3);
		realm.add(p6, e2);
		realm.add(p2, e1);
		end=nanoTime();
		for(int i=0; i<100000L; ){
			i++;
		}
		realm.addCommand("Unpause");
		
		
		out.println("Insertion of one components took "+(double)(end-start)/6000000000L);
		
		out.println("Add component");
		realm.getPriority();
		realm.getCompMan().remove(e2);
		
		realm.addCommand("Pause");
		for(int i=0; i<10000000; i++)
			i++;
		//out.println("Pause in main");
		realm.addCommand("Unpause");
		///out.println("Unpause from main");
		//realm.addCommand("Sleep: 1000");
		for(int i=0; i<1000000000; i++)
			i++;
		out.println(realm.toString());
		realm.addCommand("Die");
		
		/***********************/
		
	}

}
