import Core.Component;
import Core.Entity;
import Core.CompType;
import Core.Realm;


public class TestComponent extends Component{

	private int update;
	
	
	public TestComponent(Realm r) {
		super(TestComponent.class,r);
		update=0;
	}
	
	public void incre(){update++;}
	public int getUpdate(){return update;}

}
