package Core;
import Core.Exceptions.NoEntityException;
import Core.Managers.*;
import util.structures.Queue;
import static java.lang.System.out;


/**
 * <p>
 * 	This is the backbone of the CBE System. It houses
 * 	the managers for all entities, components, and 
 * 	systems. All data traffic is handled through this class.
 * 	To control the realm a command queue has been added so
 * 	that commands can be issued to the realm and then acted
 * 	on at the end of every cycle through the systems.
 * </p>
 * 	Legal commands so far are:<br>
 * 		Pause: Pauses the execution of systems<br>
 * 		Unpause: Continues the execution of systems<br>
 * 		Die: Ends this Relam but does not destroy data;<br>
 * 		Sleep: Sleep command pauses the execution of the Realm and is formated "Sleep: milliseconds"<br>
 * 		<center>Example: Sleep: 500</center>
 * 		Delay: Delay command changes the number of milliseconds that the realm sleeps between cycles and is formated like the Sleep command.<br>
 * 		<center>Example: Delay: 500</center>
 * 
 * @author James H Birchfield IV
 * @author Regan Prater
 * @since 1.0
 * @version 1.1
 */



public class Realm extends Thread{
	
	private SystemManager SysManager;
	private EntityManager EntManager;
	private ComponentManager CompManager;
	private GroupManager GroupManager;
	private boolean Dead=false;
	private boolean Pause=false;
	private Queue<String> Command;
	private Queue<System> external;
	private long delay = 20; 
	
	public Realm(int sz, int pr){
	
		SysManager= new SystemManager(sz);
		EntManager = new EntityManager(sz);
		CompManager = new ComponentManager(sz);
		GroupManager = new GroupManager(sz);
		Command = new Queue<String>();
		external = new Queue<System>();
		this.setDaemon(false);
		
		if(pr>Thread.MIN_PRIORITY && pr<Thread.MAX_PRIORITY)
			this.setPriority(pr);
		else if(pr>Thread.MAX_PRIORITY)
			this.setPriority(MAX_PRIORITY);
		else
			this.setPriority(MIN_PRIORITY);
		
		this.start();
	}

	public void run() {
		while(!Dead){
			
			if(!Pause){
				SysManager.effectAll();
				while(!external.isEmpty())
					external.dequeue().effect();
			}
			try {
				this.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			checkCommands();
		}
		out.println("End of Line.");
	}
	
	
	public SystemManager getSysMan(){return SysManager;}
	public EntityManager getEntMan(){return EntManager;}
	public ComponentManager getCompMan(){return CompManager;}
	public GroupManager getGroupMan(){return GroupManager;}
	
	public void add(System s){SysManager.add(s);}
	public void add(Entity e){EntManager.add(e);}
	public void add(String name, Group g){ GroupManager.add(name, g); }
	public void add(Component c, Entity e){
		try {
		e.addComponent(c);
	} catch (NoEntityException e1) {
		e1.printStackTrace();
	}}
	public void addCommand(String command){Command.enqueue(command);}
	public void addActiveSystem(System active){ external.enqueue(active); }
	
	private void checkCommands(){
		//out.println("Checking commads");
		while(!Command.isEmpty()){
			String comm = Command.dequeue();
			
			if(comm.equals("Pause"))
				Pause=true;
			else if(comm.equals("Unpause"))
				Pause=false;
			else if(comm.equals("Die"))
				Dead=true;
			else if(comm.contains("Sleep")){
				comm = comm.split(": ")[1];
				try {
					this.sleep(Long.parseLong(comm));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}//end Sleep Command
			else if(comm.contains("Delay")){
				comm = comm.split(": ")[1];
				delay = Long.parseLong(comm);
			}//end Delay Command
				
			
		}//end While loop
		
		
		
	}//end checkCommands
	
	
	public String toString(){
		StringBuilder ret = new StringBuilder(CompManager.toString().length()*3);
		ret.append("\nSystem Manager: "+SysManager.toString()+"\n");
		ret.append("Component Manager: "+CompManager.toString()+"\n");
		ret.append("Entity Manager: "+EntManager.toString()+"\n");
		return ret.toString();
	}
	
}//end Realm
