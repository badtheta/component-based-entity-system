package Core;

import java.util.BitSet;

import Core.Exceptions.NoEntityException;
import Core.Managers.ComponentManager;
import static java.lang.System.out;


/**
 * An identifier for components
 * 
 * @author James H. Birchfield IV
 * @author Regan Prater
 * @since 1.0
 * @version 1.1
 *
 */

public class Entity {
	public static Entity Null = new Entity(-1,null);
	protected long id;
	protected int NumComponents;
	protected Realm realm;
	protected BitSet componentTypes;
	
	public Entity(Realm r){
		id=-1;
		realm = r;
		NumComponents=0;
		componentTypes=new BitSet();
	}
	public Entity(long i, Realm r){
		id=i;
		realm = r;
		NumComponents=0;
		componentTypes=new BitSet();
	}
	
	public long getId(){return id;}
	public int getNumComponents(){return NumComponents;}
	public BitSet getCompTypes(){return componentTypes;}
	public void setID(long i){id=i;}
	public void addComponent(Component c) throws NoEntityException{
		c.setEntity(this);
		//realm.getCompMan().register(c.getClass());
		componentTypes.set(c.TypeId.getIndex());
		//realm.getCompMan().register(c.getClass());
		NumComponents++;
		realm.getCompMan().Insert(c);
	}
	public void NumCompsDec(){NumComponents--;}
	/**
	 * Removes all Components that belong to this entity
	 * from the static ComponentManager
	 */
	public void End(){realm.getEntMan().remove(this);}
	
	public Realm getRealm(){return realm;}
	
	public String toString(){return ""+id;	}
	
}//end Entity
