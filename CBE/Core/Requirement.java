package Core;
import Core.Managers.ComponentManager;
import java.util.BitSet;

/**
 * 
 * A set of Bitsets for the fast comparison of
 * what components are needed for a process.
 * 
 * @author James H. Birchfield IV
 * @since 1.0
 * @version 1.1
 *
 */


public class Requirement {
	
	private BitSet All;
	private BitSet Exclude;
	private BitSet One;
	private Realm r;
	
	public Requirement(Realm m){
		r=m;
		All=new BitSet();
		Exclude=new BitSet();
		One=new BitSet();
	}
	
	public void all(Class<? extends Component>... types){
		for(Class<? extends Component> c : types)
			All.set(r.getCompMan().get(c).getIndex());
	}
	public void exclude(Class<? extends Component>... types){
		for(Class<? extends Component> c : types)
			Exclude.set(r.getCompMan().get(c).getIndex());
	}
	public void one(Class<? extends Component>... types){
		for(Class<? extends Component> c : types)
			One.set(r.getCompMan().get(c).getIndex());
	}
	
	public BitSet getAll(){return All;}
	public BitSet getExclude(){return Exclude;}
	public BitSet getOne(){return One;}
	
	/**
	 * Checks other to see if it meets all the requirements set by All
	 * @param other The BitSet to be checked
	 * @return True if all classes set in All are present in other
	 */
	public boolean meetsAll(BitSet other){
		BitSet tst = (BitSet) other.clone();
		tst.and(All);
		return tst.equals(All);
	}
	/**
	 * Checks other for any of the classes set in Exclude
	 * @param other The BitSet to be checked
	 * @return True if none of the classes set in Exclude are present in other
	 */
	public boolean meetsExclude(BitSet other){
		if(Exclude.isEmpty())
			return true;
		return !other.intersects(Exclude);
	}
	/**
	 * Checks other for at least one class set in One
	 * @param other The BitSet to Be checked
	 * @return True if at least one class in other matches one class in One 
	 */
	public boolean meetsOne(BitSet other){
		if(One.isEmpty())
			return true;
		return other.intersects(One);
	}
	/**
	 * Checks check for all requirements set by this Requirement
	 * @param check The BitSet to be checked
	 * @return True if check meets all requirements
	 */
	public boolean meetsRequirements(BitSet check){
		return meetsAll(check) && meetsExclude(check) && meetsOne(check);
	}
	
	public String toString(){
		return "All: "+All+"\nExclude: "+Exclude+"\nOne: "+One;
	}
	
}//end Requirement
