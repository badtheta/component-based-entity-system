package Core;

import java.util.HashMap;

import Core.Exceptions.NoEntityException;


/**
 * A contianer to hold together Entities and their components
 * 
 * @author James H. Birchfield IV
 * @since 1.1
 * @version 1.1
 *
 */


public class Group {
	
	public static Group Null = new Group(-1);
	
	protected HashMap<String,Entity> group;
	
	
	
	public Group(){group = new HashMap<String, Entity>(10);}
	
	private Group(int i){
		group = new HashMap<String, Entity>(i);
	}
	
	/**
	 * Gets the Entity out of this group
	 * @param name The name of the entity in this group
	 * @return The Entity by 'name' if name is not in this group, will return Entity.Null 
	 */
	public Entity getMember(String name){
		if(group.containsKey(name))
			return group.get(name);
		return Entity.Null;
	}
	/**
	 * Adds the entity under the specified name to the group
	 * @param name Key used for the entity
	 * @param ent Entity to add
	 * @throws NoEntityException Thrown if ent == Entity.Null
	 */
	public void addMember(String name, Entity ent) throws NoEntityException{
		if(ent.equals(Entity.Null))
			throw new NoEntityException();
		else
			group.put(name, ent);
	}
	
	public Entity[] getAll(){
		return (Entity[]) group.values().toArray();
	}
	
	
	
}//end Group
