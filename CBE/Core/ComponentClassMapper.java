package Core;

import java.lang.reflect.Array;
import java.util.ArrayList;
import static java.lang.System.out;
import static java.lang.System.err;
import Core.Managers.ComponentManager;
import static java.lang.System.arraycopy;


/**
 * Allows for the Realm to keep track of components.
 * 
 * @author James H. Birchfield IV
 * @since 1.0
 * @version 1.1
 *
 * @param <T> The class of component this mapper will get
 */


public class ComponentClassMapper<T extends Component>{

	private CompType type;
	private Class<T> classType;
	private Realm manager;
	
	public ComponentClassMapper(Class<T> t, Realm m){
		manager=m;
		type = manager.getCompMan().get(t);
		classType=t;
	}
	/**
	 * Gets the components matching this ClassMapper from entity e
	 * @param e The entity to have its components extracted
	 * @return The Components in e that match the type of this ClassMapper
	 */
	public T[] getFromEntity(Entity e){
		Component[] comps = e.getRealm().getCompMan().get(e);//get e's components
		ArrayList<T> ret = new ArrayList<T>(1);//create an arraylist of the return type
		
			for(Component c: comps){
				//out.println(c);
				if(c!=null && c.getTypeId().getIndex()==type.getIndex() ){//if the component matches this class type then add to return
					//out.println(c+"  Compare to "+classType);
					ret.add((T) c);//cast it as the return type
				}
				
			}
			//out.println("Return array  "+ ret);
		T[] retur = ((T[])Array.newInstance(classType, ret.size()));//create a new array that matches this class type
		arraycopy(ret.toArray(),0,retur,0,ret.size());//accelerated copy of the components from the arraylist to the return array.
		
		return retur;
	}

	
	
}//end ComponentClassMapper
