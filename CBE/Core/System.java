package Core;

import Core.Managers.ComponentManager;
import Core.Managers.EntityManager;

/**
 * The logic containers of the program. Systems act 
 * can do anything to the entities and components of 
 * the realm that they are contained it. They simply need
 * to access them with by filtering the components in the 
 * realm using their requirements.
 * 
 * @author James H. Birchfield IV
 * @since 1.0
 * @version 1.1
 *
 */


public abstract class System{

	protected Requirement requirements;
	protected Realm place;
	protected boolean passive;
	//protected int id;
	 /**
	  * Constructor
	  * @param p Boolean to tell main loop to run the System
	  * @param r Realm the System will be added to
	  * @param req Processing requirements of the System
	  */
	public System(boolean p, Realm r, Requirement req){
		passive=p;
		place=r;
		requirements = req;
	}
	/**
	 * Constructor
	 * @param p Boolean to tell the main loop to run the System
	 * @param r Realm the System will be added to
	 */
	public System(boolean p,Realm r){
		passive=p;
		place=r;
		requirements = new Requirement(place);
	}
	
	public abstract void effect();
	
	public void setRequirements(Requirement req){requirements=req;}
	//public void setID(int i){id=i;}
	
	public boolean isPassive(){return passive;}
	//public int getId(){return id;}
	
	public Requirement getRequirements(){return requirements;}
	
	public String toString(){
		return /*"ID: "+id+*/"\nRequirements:\n"+requirements+"\nPassive: "+passive;
	}
	
}//end System
