package Core;

import Core.Managers.ComponentManager;
import static java.lang.System.out;


/**
 * 
 * Base class for all data containers. Components should only contain data<br>
 * and functions to get and set that data. 
 * 
 * @author James H. Birchfield
 * @since 1.0
 * @version 1.1
 *
 */

public abstract class Component {

	protected Entity EntityId;
	protected final CompType TypeId;
	protected Realm manager;
	
	public Component(Class<? extends Component> t, Realm r){
		EntityId=Entity.Null;
		manager = r;
		r.getCompMan().register(t);
		TypeId=r.getCompMan().get(t);
	}
	public void setEntity(Entity e){EntityId = e;}
	
	public Entity getEntity(){return EntityId;}
	
	public boolean hasTypeID(){return !TypeId.equals(null);}
	
	public CompType getTypeId(){return TypeId;}
	
	
	public String toString(){
		return "Component< "+EntityId.getId()+";"+TypeId+"> ";
	}
	
}//end Component
