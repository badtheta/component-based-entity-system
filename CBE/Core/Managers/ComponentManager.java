package Core.Managers;
import Core.Component;
import Core.Entity;
import Core.CompType;
import Core.Exceptions.*;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * 
 * Holds all components that are legal to use
 * within the realm that contains this manager.
 * 
 * @author James H. Birchfield IV
 * @since 1.0
 * @version 1.1
 *
 */

public class ComponentManager {

	protected int index=0;
	protected ArrayList<Component> Comps;
	protected HashMap<Class<? extends Component>, CompType> register = new HashMap<Class<? extends Component>,  CompType>(10);
	
	
	public ComponentManager(int sz){
		Comps = new ArrayList<Component>(sz);
	}
	
	 /**
	  * Inserts a Component into the manager by entity and type
	  * @param c Component to be added to the manager
	  * @throws NoEntityException Thrown when c does not have an Entity
	  */
	public void Insert(Component c) throws NoEntityException{
		if(c.getEntity()==Entity.Null || c.getEntity().getId()<0){
			throw new NoEntityException("Component does not contain an entity!: "+c.getTypeId());
		}//check for valid entity
		long EId = c.getEntity().getId();//id of entity
		
		if(Comps.size()==0){
			//System.out.println("Forced to front  "+c);
			Comps.add(c);
		
			return;
		}
		if(Comps.size()==1){
			if(EId > Comps.get(0).getEntity().getId())
				Comps.add(c);
			else if(EId == Comps.get(0).getEntity().getId())
				if(c.getTypeId().getIndex() >= Comps.get(0).getTypeId().getIndex()){
					Comps.add(c);
					//System.out.println("Added to end");
					return;
				}
				else
					Comps.add(0,c);
			else
				Comps.add(0, c);
			//System.out.println("Added to front");
			return;
		}
		if(c.getEntity().getId() >= Comps.get(Comps.size()-1).getEntity().getId() && c.getTypeId().getIndex() >= Comps.get(Comps.size()-1).getTypeId().getIndex()){
			Comps.add(c);
			//System.out.println("Forced to end: "+c);
			return;
		}
		if(c.getEntity().getId() <= Comps.get(0).getEntity().getId() && c.getTypeId().getIndex() <= Comps.get(0).getTypeId().getIndex()){
			Comps.add(0,c);
			//System.out.println("Forced to front: "+c);
			return;
		}	
		
		int EntityIndex= getStartOf(EId);
		
		for(int j=0; j<=c.getEntity().getNumComponents() && EntityIndex<Comps.size()-1 ; EntityIndex++, j++){
			if(c.getEntity().getId() <= Comps.get(EntityIndex).getEntity().getId()){
				if(c.getTypeId().getIndex() <= Comps.get(EntityIndex).getTypeId().getIndex()){
					//System.out.println("Added "+c+"  to "+(EntityIndex));
					Comps.add(EntityIndex, c);
					//System.out.println("Component System : "+Comps);
					return;
				}
				else if( c.getTypeId().getIndex() > Comps.get(EntityIndex).getTypeId().getIndex() && j==c.getEntity().getNumComponents()-1){
					//System.out.println("Added "+c+"  to "+(EntityIndex));
					Comps.add(EntityIndex, c);
					//System.out.println("Component System : "+Comps);
					return;
				}
			}
			else{
				Comps.add(EntityIndex-1, c);
				//System.out.println("Was added to  "+(EntityIndex-1));
				//System.out.println("Component System : "+Comps);
				return;
			}
		}//end for loop	
		Comps.add(EntityIndex , c);
		//System.out.println("Was added to  "+EntityIndex);
		//System.out.println("Component System : "+Comps);
		/**/
	}//insert a component into the system
	/**
	 * Removes all Components of type t from Entity e
	 * @param t Type to remove
	 * @param e Entity to remove from
	 */
	public void remove(CompType t, Entity e){
		int ident = t.getIndex();
		int i = getStartOf(e.getId());
		
				for(int j=0; j<e.getNumComponents(); j++){
					if(ident ==Comps.get(j+i).getTypeId().getIndex()){
						e.NumCompsDec();
						Comps.remove(i+j);
						j--;
					}
				}//end for loop
			
		
	}
	/**
	 * Removes all components belonging to Entity e
	 * @param e Entity to remove
	 */
	public void remove(Entity e){
		int i = getStartOf(e.getId())-1;
				for(int j=0; j<e.getNumComponents(); j++){
						e.NumCompsDec();
						Comps.remove(i+j);
						j--;
					
				}//end for loop
				
	}
	
	/**
	 *Returns all components belonging to Entity e
	 * @param e Entity to get
	 */
	public Component[] get(Entity e){
		Component[] ret= new Component[e.getNumComponents()];
		if(Comps.isEmpty())
			return ret;
		//System.out.println("In Get Func: "+e+"  Num components: "+e.getNumComponents());
		int i= getStartOf(e.getId());		
		for(int j=0; j<e.getNumComponents() && j<ret.length && (i+j)<Comps.size() ; j++){
						//System.out.println("inside loop: I: "+i+" J: "+j);
						ret[j]= 
								Comps.get(i+j);
					
				}//end for loop
				
		return ret;
	}
	
	
	
	public String toString(){
		int num = Comps.get(0).toString().length()*Comps.size();
		StringBuilder ret = new StringBuilder();
		for(Component arg: Comps)
			ret.append(arg+":");
		
		return ret.toString();
	}
	
	
	/**
	 * Gives the Class type a valid uniqe ideifier
	 * @param type The Class Type to register
	 */
	public void register(Class<? extends Component> type){
		if(!register.containsKey(type))
			register.put(type,new CompType(index++, type));
		
	}
	/**
	 * Returns the index associated with the Component class type
	 * @param type Class type for the Component
	 * @return Index of the Class type
	 */
	public CompType get(Class<? extends Component> type){
		
		if(!register.containsKey(type))
			register(type);
		
		return register.get(type);
		
	}
	
	/**
	 * Uses binary search of the ArrayList to get first index of an Entity in the list
	 * @param EId ID of the Entity to search for
	 * @return The starting index of the Entity with ID EId
	 */
	private int getStartOf(long EId){
		int EntityIndex=-1;
		//System.out.println("//****************Binary Search************/");
		int l=0, r=Comps.size()-1, mid = -1;
		
				
		while(l <= r && mid != l && mid != r){
			mid= (l+((r-l)/2));
			if(Comps.get(mid).getEntity().getId() == EId || mid==r || mid==l){//if entity Id = mid then break
				EntityIndex = mid;
				break;
			}
			else if(EId < Comps.get(mid).getEntity().getId())
				r=mid-1;
			else if(EId > Comps.get(mid).getEntity().getId())
				l=mid+1;
			//System.out.println("Left: "+l+"   Right: "+r+"   Mid: "+mid);
		}
		//System.out.println("/*******************End Binary Search*******************/");
		//System.out.println("Index chossen "+mid);
		//System.out.println("/*********************Finding begining of Entity*****************");
	
		if(EntityIndex !=-1 && Comps.get(EntityIndex).getEntity().getId() != EId){
			while(EntityIndex !=-1 && Comps.get(EntityIndex).getEntity().getId() == EId){//go back to beginning of Entity's space
				//System.out.println("Current Entity ID: "+Comps.get(EntityIndex).getEntity().getId());
				//System.out.println("Entity ID: "+EId);
				EntityIndex--;
			}
			++EntityIndex;
		}
		//System.out.println("/*********************End search*****************");
		
		//System.out.println("Start of Entity: "+EId+"    Index: "+(EntityIndex));
			return EntityIndex;//start at entity's space
	}//end getStartOf
	
	
}//end ComponentManager
