package Core.Managers;

import java.util.ArrayList;

import Core.Component;
import Core.Entity;
import Core.Requirement;
import Core.System;
import Core.Exceptions.NoEntityException;
import Core.Exceptions.NoSystemException;

/**
 * Contains all of the Systems in a Realm.
 * 
 * @author James H. Birchfield
 * @since 1.0
 * @version 1.1
 *
 */

public class SystemManager {
	
	//protected int index=0;
	protected ArrayList<System> systems;
	
	public SystemManager(int i){systems= new ArrayList<System>(i);}
	
	public void add(System s){
		if(systems.contains(s))
			return;
		//s.setID(index++);
		systems.add(s);
	}
	
	public void add(System s, int index){
		if(systems.contains(s))
			return;
		//s.setID(index++);
		systems.add(index, s);
	}
	
	public void remove(System s){systems.remove(s);}
	
	public void effectAll(){
		if(!systems.isEmpty())
		for(System s: systems)
			if(s.isPassive())
				s.effect();
	}
	
	public String toString(){
		int num = systems.get(0).toString().length()*systems.size();
		StringBuilder ret = new StringBuilder();
		for(System arg: systems)
			ret.append(arg+":");
		
		return ret.toString();
	}
	
}//end SystemMangager
