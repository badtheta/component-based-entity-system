package Core.Managers;

import java.util.ArrayList;

import Core.Component;
import Core.Entity;
import Core.Requirement;
import Core.CompType;
import Core.Exceptions.NoEntityException;

/**
 * Manages all Entities in the application
 * 
 * @author James H. Birchfield IV
 * @since 1.0
 * @version 1.1
 *
 */

public class EntityManager {

	protected long currentID=0;
	protected ArrayList<Entity> entities;
	/**
	 * Constructor
	 * @param sz size of the ArrayList held by the manager
	 */
	public EntityManager(int sz){entities = new ArrayList<Entity>(sz);}
	/**
	 * Adds an Entity to this Manager and gives it a unique Identifier
	 * @param e The Entity to be added
	 */
	public void add(Entity e){
		e.setID(currentID);
		currentID++;
		entities.add(e);
	}
	/**
	 * Calls e's End() and removes it from the the EntityManager
	 * @param e The Entity to be removed
	 */
	public void remove(Entity e){
		e.End();
		entities.remove(e);
		}
	/**
	 * Removes the Entity with EId from this manager as well as its associated components from the ComponentManager
	 * @param EId The desired Entity to remove
	 * @return The Entity if the Manager contains it
	 * @throws NoEntityException if the EId is not a valid Entity ID
	 */
	public Entity remove(long EId) throws NoEntityException{
		int l=0, r=entities.size()-1;
		while(l<=r){
			int mid= l+(r-l)/2;
			if(EId< entities.get(mid).getId())
				r=mid-1;
			else if(EId > entities.get(mid).getId())
				l=mid+1;
			else if(entities.get(mid).getId()==EId){
				Entity ret = entities.get(mid);
				entities.get(mid).End();
				entities.remove(mid);
				return ret;
			}
			else
				throw new NoEntityException(EId+" does not exist in this Manager");
		}
		throw new NoEntityException(EId+" does not exist in this Manager");
	}//end remove
	/**
	 * Retrieves the Entity with the associated EId from this Manager
	 * @param EId The EId of the Entity to be retrieved
	 * @return The Entity associated with the EId
	 * @throws NoEntityException if EID is not a valid Entity ID in this Manager 
	 */
	public Entity get(long EId) throws NoEntityException{
		int l=0, r=entities.size()-1;
		while(l<=r){
			int mid= l+(r-l)/2;
			if(EId< entities.get(mid).getId())
				r=mid-1;
			else if(EId > entities.get(mid).getId())
				l=mid+1;
			else if(entities.get(mid).getId()==EId){
				return entities.get(mid);
			}
			else
				throw new NoEntityException(EId+" does not exist in this Manager");
		}
		throw new NoEntityException(EId+" does not exist in this Manager");
	}
	
	public ArrayList<Entity> getEntities(Requirement req){
		ArrayList<Entity> ret = new ArrayList<Entity>(10);
		for(Entity e: entities)
			if(req.meetsRequirements(e.getCompTypes()))
				ret.add(e);
		
		return ret;
	}
	
	public String toString(){
		int num = entities.get(0).toString().length()*entities.size();
		StringBuilder ret = new StringBuilder();
		for(Entity arg: entities)
			ret.append(arg+":");
		
		return ret.toString();
	}
	
}//end EntityManager
