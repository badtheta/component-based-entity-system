package Core.Managers;

import java.util.Collection;
import java.util.HashMap;
import Core.Group;


/**
 * 
 * Holds all groups. Simply a holding place
 * 
 * @author James H. Birchfield IV
 * @since 1.1
 * @version 1.1
 *
 */


public class GroupManager {
	
	
	private HashMap<String,Group> Groups;
	
	public GroupManager(){
		Groups = new HashMap<String,Group>(10);
	}
	public GroupManager(int size){
		Groups = new HashMap<String,Group>(size);
	}
	
	public void add(String name, Group g){ Groups.put(name, g);}
	
	public Group get(String name){ return Groups.get(name); }
	
	public Group remove(String name){ return Groups.remove(name); }
	
	public Collection<Group> getValues(){ return Groups.values();}
	
	public Collection<String> getKeys(){ return Groups.keySet(); }
	
	public boolean contains(String name){ return Groups.containsKey(name); }
	
	public boolean hasCollection(Group g){ return Groups.containsValue(g); }
	
	public int size(){ return Groups.size(); }
	
}//end GroupManager
