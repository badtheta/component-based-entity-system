package Core.Exceptions;


public class NoEntityException extends Exception{

	private String error;
	
	public NoEntityException(){
		super();
		error = "Unknown";
	}
	public NoEntityException(String e){
		super(e);
		error = e;
	}
	public String getError(){return error;}
	
}//end NoEntityException
