package Core.Exceptions;

public class NoSystemException extends Exception{

	private String error;
	
	public NoSystemException(){
		super();
		error = "Unknown";
	}
	public NoSystemException(String e){
		super(e);
		error = e;
	}
	public String getError(){return error;}
}
