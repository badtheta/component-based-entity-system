package Core;


/**
 * Allows for the indexing of Component classes in the Realm
 * 
 * @author James H. Birchfield
 * @since 1.0
 * @version 1.1
 *
 */

public class CompType {

	private final int index;
	private final Class<? extends Component> type;
	
	public static CompType Null= new CompType(-1,null);
	
	public CompType(int i, Class<? extends Component> c){
		index=i;
		type=c;
	}
	
	public Class<? extends Component> getType(){return type;}
	public int getIndex(){return index;}
	
	public String toString(){
		return ""+type.getName()+"  index: "+index;
	}
	
}//end Type
