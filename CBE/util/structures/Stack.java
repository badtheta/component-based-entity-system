package util.structures;

import static java.lang.System.arraycopy;

public class Stack<T>{

	private T[] list;
	private int max;
	private int lead=-1;
	
	public Stack(){
		max=10;
		list = extracted(max);
	}
	public Stack(int s){
		max=s;
		list= extracted(max);
	}
	
	public void push(T obj){
		if(lead==max-1)
			resize();
		list[++lead]=obj;
	}
	
	public T pop(){
		if(isEmpty())
			return null;
		T tem = list[lead];
		list[lead--]=null;
		return tem;
	}
	
	public T peek(){
		if(lead>=0)
			return list[lead];
		else
			throw new NullPointerException();
	}
	
	public boolean isEmpty(){return lead==-1;}
	
	public int size(){return lead+1;}
	private void resize(){
		max*=2;
		T[] temp = extracted(max);
		arraycopy(list, 0, temp, 0, list.length);
		list = temp;	
	}
	
	private T[] extracted(int s) {
		return (T[])(new Object[s]);
	}
	
	/**
	 * Returns a String containing all elements in this Stack.
	 */
	public String toString(){
		int num = list[0].toString().length()*list.length;
		StringBuilder ret = new StringBuilder();
		for(T arg: list)
			ret.append(arg+":");
		
		return ret.toString();
	}
	
}//end Stack
