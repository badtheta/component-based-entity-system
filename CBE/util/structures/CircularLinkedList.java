package util.structures;

import java.util.NoSuchElementException;


public class CircularLinkedList<T> {
	
	//last node
	protected ListNode<T> last;
	
	public CircularLinkedList(){
		last=null;
	}
	/**
	 * adds object to end of list
	 * @param c T to add
	 */
	public void add(T c){
		if(last==null){
		last=new ListNode<T>(c, null);
		last.setNext(last);
		}
		else{
			last.setNext(new ListNode<T>(c, last.getNext()));
			last = last.getNext();
		}//end last
	}
	/**
	 * Adds object at index
	 * @param index Place to add object
	 * @param o Object to add
	 */
	public void add(int index, T o){
		if(index<0 || index>size())
			throw new NoSuchElementException("Index is out of bounds");
		if(last==null){
			last=new ListNode<T>(o);
			last.setNext(last);
			return;
		}
		if(index==0){
			addFirst(o);
			return;
		}
		if(index==size()-1)
			add(o);
		
		ListNode<T> curr=last.getNext();
		
		do{
			curr=curr.getNext();
			index--;
			//System.out.println(index);
			if(index==1)
				break;
				
		}while(index>0);
		
		//System.out.print(curr+"  ");
		//System.out.println(curr.getNext()+"  ");
		ListNode<T> Nxt = curr.getNext();
		ListNode<T> temp = new ListNode<T>(o, Nxt);
		curr.setNext(temp);
	}
	/**
	 * Adds Object to front of list
	 * @param c Object to add
	 */
	public void addFirst(T c){
		if(last==null){
			last=new ListNode<T>(c, null);
		last.setNext(last);
		}
		else{
			last.setNext(new ListNode<T>(c, last.getNext()));
		}//end last
	}
	/**
	 * Removes the first object
	 * @return First object in list
	 */
	public T removeFirst(){
		T ret = (T) last.getNext().getItem();
		if(size()==1)
			last=null;
		else if(size()==0)
			return null;
		else
			last.setNext(last.getNext().getNext());
			
		return ret;
	}
	/**
	 * Remove last object in list
	 * @return
	 */
	public T removeLast(){
		
		T ret=last.getItem();
		if(size()==1){
			last=null;
			return ret;
		}
		
		ListNode<T> curr=last.getNext();
		
		while(curr.getNext()!=last)
			curr=curr.getNext();
		
		curr.setNext(last.getNext());
		last=curr;
		return ret;
		
	}
	/**
	 * Removes object from list
	 * @param index Index of the object to be removed
	 * @return Object removed
	 */
	public T remove(int index){
		if(index<0 || index>size())
			throw new NoSuchElementException("Index is out of bounds");
		else if(size()==0)//empty list
			return null;
		else if(size() == 1 || index==0){//remove first
			T ret = last.getNext().getItem();
			removeFirst();
			return ret;
		}
		else if(index==size()-1){//remove last
			T ret = last.getItem();
			removeLast();
			return ret;
		}
		
		ListNode<T> curr=last;
		T ret=null;
		do{
			curr=curr.getNext();
			//System.out.println(index);
			if(index==1){
				ret = (T) curr.getNext().getItem();
				curr.setNext(curr.getNext().getNext());
				return ret;
			}
			index--;
		}while(index>0);
		
		return ret;
		
	}
	/**
	 * Gets requested object
	 * @param index Index of desired Object
	 * @return Object at index
	 */
	public T get(int index){
		if(index<0 || index>size())
			throw new NoSuchElementException("Index is out of bounds");
		if(index==0)
			return (T) last.getNext().getItem();
		
		ListNode<T> curr=last.getNext();
		T ret=null;
		do{
			curr=curr.getNext();
			index--;
			//System.out.println(index);
			if(index==0)
				return (T) curr.getItem();
			
		}while(index>0);
		
		return ret;
	}//end get
	/**
	 * Finds size of list
	 * @return Size of list
	 */
	public int size(){
		if(last==null)
			return 0;
		
			ListNode<T> curr=last;
			int ret = 0;
			do{
				curr=curr.getNext();
				ret+=1;
			}while(curr!=last);
			return ret;
	}//end size
	/**
	 * Changes object at index
	 * @param index Index to change
	 * @param o Object to set index to
	 */
	public void set(int index, T o){
		if(index<0 || index>size())
			throw new NoSuchElementException("Index is out of bounds");
		if(last==null)
			return;
		
		ListNode<T> curr=last.getNext();
		
		while(index>=0){
			if(index==0)
				curr.setItem(o);
			curr=curr.getNext();
			index--;
		}
	}
	/**
	 * Turns The list into an array
	 * @return Array form of the list
	 */
	public T[] toArray() {
		T[] ret = (T[]) new Object[size()];
		ListNode<T> cur = last.getNext();
		int index=0;
		
		while(cur!=last.getNext()){
			ret[index]=cur.getItem();
			cur=cur.getNext();
		}
		
		return null;
	}
	/**
	 * Clears all objects from the list
	 */
	public void clear(){last=null;} 
	/**
	 * Determines if list is empty
	 * @return Boolea representing list being empty
	 */
	public boolean isEmpty(){
		return size()==0;
	}
	
	public String toString(){
		int num = last.toString().length()*size();
		StringBuilder ret = new StringBuilder(num);
		
		if(last==null)
			return ret.append("[Empty]").toString();
		ListNode<T> curr=last;
		do{
			curr=curr.getNext();
			ret.append(" "+curr.getItem()+":");
		}while(curr!=last);
		
		return ret.toString();
	}//end toString
	
	
	
}//end CircularLinkedList
