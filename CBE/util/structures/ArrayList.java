package util.structures;


import static java.lang.System.arraycopy;


public class ArrayList<T>{

	protected T[] data;
	protected int size = 0;
	
	public ArrayList(){ data = getGenericArr(10); }
	public ArrayList(int size){ data = getGenericArr(size); }
	public ArrayList(T... items){ data=items;}
	
	/**
	 * Gets the number of elements in the array
	 * @return The number of elements in the array
	 */
	public int size(){return size;}
	/**
	 * Adds the item to the index represented by its logical size
	 * @param item Item to add
	 */
	public void add(T item){
		if(size==data.length-1)
			resize();
		data[size++]=item;
	}//end add(T)
	/**
	 * Adds the item to the ArrayList at the appropriate place
	 * @param item Item to add
	 * @param place Index in the array to add to
	 */
	public void add(T item, int place){
		if(place < 0  ||  place > data.length)
			throw new ArrayIndexOutOfBoundsException();
		if(size==data.length-1)
			resize();
		if(data[place]==null){
			data[place]=item;
			size++;
			return;
		}
		
		T[] newData = getGenericArr((int)(data.length));
		
		if(place == 0){
			newData[0] = item;
			arraycopy(data, 0, newData, 1, data.length);
			data = newData;
			size++;
			return;
		}
		
		arraycopy(data, 0, newData, 0, place-1);
		newData[place-1] = item;
		arraycopy(data, place-1, newData, place, data.length-place-1);
		data = newData;
		size++;
	}//end add(T,int)
	/**
	 * Removes a item from the end of the ArrayList
	 * @return The item from the end of the ArrayList
	 */
	public T remove(){ 
		T ret = data[size]; 
		data[size--]=null;
		return ret;
	}
	/**
	 * Removes the item at the specified index
	 * @param place Index of item to remove
	 * @return The item that was removed
	 */
	public T remove(int place){
		if(place < 0  ||  place > data.length)
			throw new ArrayIndexOutOfBoundsException();
		
		T ret = data[place];
		T[] newData = getGenericArr((int)(data.length));
		
		if(place == 0){
			arraycopy(data, 1, newData, 0, data.length);
			data = newData;
			size--;
			return ret;
		}
		
		arraycopy(data, 0, newData, 0, place);
		arraycopy(data, place, newData, place-1, data.length-place);
		data = newData;
		size--;
		
		
		return ret;
	}//end remove(int)
	/**
	 * Gets the item at place from the ArrayList
	 * @param place Index of item to get
	 * @return Item at place in the ArrayList
	 */
	public T get(int place){
		if(place < 0  ||  place > data.length)
			throw new ArrayIndexOutOfBoundsException();
		return data[place];
	}
	/**
	 * Changes the index: place to the specified item
	 * @param item Item to set index to
	 * @param place Index to change
	 */
	public void set(T item, int place){
		if(place < 0  ||  place > data.length)
			throw new ArrayIndexOutOfBoundsException();
		data[place] = item;
	}
	/**
	 * Returns the data
	 * @return
	 */
	public T[] getAsArray(){
		return data;
	}
	/**
	 * Returns a String containing all elements in this ArrayList.
	 * Including nulls
	 */
	public String toString(){
		int num = data[0].toString().length()*data.length;
		StringBuilder ret = new StringBuilder();
		for(T arg: data)
			ret.append(arg+":");
		
		return ret.toString();
	}
	/**
	 * Searches the ArrayList for the specified object
	 * @param object The object to find
	 * @return True if the object is in this array, False if not
	 */
	public boolean Contains(T object){
		for(int i=0; i<data.length; i++){
			if(data[i]==null)
				continue;
			if(data[i].equals(object))
				return true;
		}
		return false;
	}//end Contains(T)

	/*********Private Functions************/

	private void resize(){
		//System.out.println("Resized: "+data.length);
		T[] newData = getGenericArr(data.length+10);
		arraycopy(data, 0, newData, 0, data.length);
		data = newData;
	}
	
	
	private T[] getGenericArr(int sz) {
		return (T[]) new Object[sz];
	}	
}//end ArrayList
