package util.structures;

import java.util.*;

public class LinkedList<T>{

	protected ListNode<T> first;
	protected int size=0;
	
	/**
	 * Constructor: Creates new Linked List
	 * @param f First and Last Node in the List
	 */
	public LinkedList(ListNode<T> f){
		first=f;
		size++;
	}
	
	public LinkedList(){
		first=null;
		
	}
	/**
	 * Add to front of list
	 * @param obj object to add
	 */
	public void addFirst(T obj){
		if(size==0){
			ListNode<T> add = new ListNode<T>(obj);
			first= add;
		}
		else{
			ListNode<T> f = new ListNode<T>(obj, first); 
			first= f;
		}
		size++;
	}
	
	/**
	 * Adds object
	 * @param obj Object to add
	 * @return true if sucessful
	 */
	public boolean add(T obj) {
		add(size, obj);
		return true;
	}
	/**
	 * Adds object at index
	 * @param index Index to add to
	 * @param obj Object to add
	 */
	public void add(int index, T obj) {
		if(index<0 || index>size)
			throw new NoSuchElementException("Index is out of bounds");
		ListNode<T> itter= first;
		for(int i=0; i<=size; i++){
			if(index==0){
				System.out.println("add at 0");
				addFirst(obj);
				break;
			}
			else if(i==size){
				System.out.println("add at end");
				add(obj);
				break;
			}
			else if(i==index-1){
				System.out.println("add at "+i);
				ListNode<T> tempNext = itter.getNext();
				ListNode<T> add = new ListNode<T>(obj, tempNext);
				itter.setNext(add);
				size++;
				break;
			}
			else
				itter=itter.getNext();
		}
	}//end add

	/**
	 * Adds all objects in the Collection to list
	 * @param col Collection to add
	 * @return True if sucessful
	 */
	public boolean addAll(Collection<T> col) {
		Iterator<T> itter=col.iterator();
		while(itter.hasNext())
			add(itter.next());
		return true;
	}

	/**
	 * Adds all objeccts in the Collection up to the limit
	 * @param lim Max index of objects to add
	 * @param col Collection to add from
	 * @return true if sucessful
	 */
	public boolean addAll(int lim, Collection<T> col) {
		int i=0;
		Iterator<T> iter = col.iterator();
		while(iter.hasNext() && i<lim){
			add(iter.next());
			i++;
		}
		return true;
	}

	/**
	 * Clears the list
	 */
	public void clear() {
		first=null;
	}

	/**
	 * Determines if the list contains an object
	 * @param obj Object to check for
	 * @return True if the list contains the object
	 */
	public boolean contains(T obj) {
		ListNode<T> cur = first;
		while(cur!=null){
			if(cur.getItem().equals(obj))
				return true;
			cur=cur.getNext();
		}
		return false;
	}

	/**
	 * Deterimines if all objects in the collection are in this list
	 * @param col Collection to check
	 * @return True if all objects in the collection are in this list
	 */
	public boolean containsAll(Collection<T> col) {
		Iterator<T> iter = col.iterator();
		while(iter.hasNext())
			if(!contains(iter.next()))
				return false;
		return true;
	}

	/**
	 * Returns the object at index 
	 * @param index Index to be returned
	 * @return Object at index
	 */
	public T get(int index) {
		if(index<0 || index>size)
			throw new NoSuchElementException("Index is out of bounds");
		ListNode<T> itter= first;
		for(int i=0; i<=size; i++){	
			if(i==index){
				return itter.getItem();
			}
			itter=itter.getNext();
		}
			return null;
	}

	/**
	 * Finds the object and returns its index
	 * @param obj Object to search for
	 * @return Returns index of object -1 if is not contained
	 */
	public int indexOf(T obj) {
		ListNode<T> cur = first;
		int i=-1;
		while(!cur.getItem().equals(obj)){
			i++;
			cur=cur.getNext();
		}
		if(i==size())
			i=-1;
		return i;
	}

	/**
	 * Determines if the list is empty
	 * @return True if list is empty
	 */
	public boolean isEmpty() {	
		return size==0;
	}
	/**
	 * Determins the last index of the object
	 * @param obj Object to search for
	 * @return Last index of the object
	 */
	
	public int lastIndexOf(T obj) {
		int i=0;
		int index=-1;
		ListNode<T> cur = first;
		while(cur.getNext()!=null){
			if(cur.getItem().equals(obj))
				index=i;
			i++;
		}
		return index;
	}
	/**
	 * Removes all instancces of the object from the list
	 * @param obj Object to remove
	 * @return boolean if sucessful
	 */
	
	public boolean remove(T obj) {
		if(size ==2){
			if(first.getItem().equals(obj))
				first=null;
		}
		
		ListNode<T> cur = first;
		
		while(cur!=null){
			if(cur.getItem().equals(obj)){
				ListNode<T> nxt=cur.getNext();
				cur.setNext(null);
				cur=nxt; 
				size--;
			}
			cur=cur.getNext();
		}
		return true;
	}
	/**
	 * Remove object at index
	 * @param index Index to remove
	 * @return Object at index
	 */
	
	public T remove(int index) {
		if(index<0 || index>size)
			throw new NoSuchElementException("Index is out of bounds");
		ListNode<T> itter= first;
		for(int i=0; i<=size; i++){	
			if(i==index-1){
				T tem= itter.getNext().getItem();
				itter.setNext(itter.getNext().getNext());
				size--;
				return tem;
			}
			itter=itter.getNext();
		}
			return null;
	}

	/**
	 * Removes the first object
	 * @return the frist object
	 */
	public T removeFirst(){
		ListNode<T> death= first;
		first=first.getNext();
		death.setNext(null);
		size--;
		return death.getItem();
	}
	/**
	 * Removes all obects in the collection from this list
	 * @param col Collection to remove
	 * @return True if sucessful
	 */
	
	public boolean removeAll(Collection<T> col) {
		Iterator<T> itter = col.iterator();
		while(itter.hasNext())
			remove(itter.next());
		return true;
	}
	/**
	 * Removes all objects not in the collection
	 * @param col Collection to save
	 * @return True if sucessful
	 */
	
	public boolean retainAll(Collection<T> col) {
		ListNode<T> cur = first;
		int i=0;
		while(cur!=null){
			Iterator<T> itter = col.iterator();
			boolean inCol=false;
			while(itter.hasNext()){
				if(cur.getItem().equals(itter.next())){
					inCol=true;
					break;
				}
			}
			if(!inCol){
				cur=cur.getNext();
				remove(i);
				i++;
			}
		}
		return true;
	}
	/**
	 * Sets index to the object
	 * @param index Index to change
	 * @param obj Object to change to
	 * @return Object previously there
	 */
	
	public T set(int index, T obj) {
		if(index<0 || index>size)
			throw new NoSuchElementException("Index is out of bounds");
		ListNode<T> itter= first;
		for(int i=0; i<size; i++){	
			if(i==index){
				T tem= itter.getItem();
				itter.setItem(obj);
				return tem;
			}
			itter=itter.getNext();
		}
			return null;
	}

	/**
	 * Finds the size of the object
	 * @return Size of the object
	 */
	public int size() {
		return size;
	}

	/**
	 * Returns a sublist of this list
	 * @param start Start index in this list
	 * @param end Ending index in this list
	 * @return A linked list of the objects from start to end
	 */
	public LinkedList<T> subList(int start, int end) {
		if(start<0 || end>size || end<0 || start>size)
			throw new NoSuchElementException("Index is out of bounds");
		
		LinkedList<T> ret = new LinkedList<T>();
		
		while(start<end){
			ret.add(get(start));
			start++;
		}
		return ret;
	}

	/**
	 * Converts the list to an array
	 * @return The array form of this list
	 */
	public T[] toArray() {
		T[] ret = (T[]) new Object[size];
		ListNode<T> cur = first;
		int index=0;
		
		while(cur!=null){
			ret[index]=cur.getItem();
			cur=cur.getNext();
		}
		
		return null;
	}


	public String toString(){
		int num = first.toString().length()*size();
		StringBuilder ret = new StringBuilder(num);
		
		if(first==null)
			return ret.append("[Empty]").toString();
		ListNode<T> curr=first;
		while(curr!=null){
			curr=curr.getNext();
			ret.append(" "+curr.getItem()+":");
		}
		
		return ret.toString();
	}
	
	
	
}//LinkedList 
