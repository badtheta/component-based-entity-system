package util.structures;

public class Queue<T>{
	
	private CircularLinkedList<T> data;
	
	public Queue(){
		data = new CircularLinkedList<T>();
	}
	
	/**
	 * Adds data to the queue
	 * @param add Adds add to the queue
	 */
	public void enqueue(T add){ data.addFirst(add);	}
	/**
	 * Removes the front of the queue
	 * @return The next node to be removed
	 */
	public T dequeue(){ return data.removeLast(); }
	/**
	 * Is the queue empty
	 * @return True if it is; False otherwise
	 */
	public boolean isEmpty(){ return data.isEmpty(); }
	/**
	 * Returns the front of the queue
	 * @return The next node in line to be removed
	 */
	public T peekFront(){ return data.get(0); }
	/**
	 * Gets the size of the queue
	 * @return The size of the queue
	 */
	public int size(){ return data.size(); }
	
	
	public String toString(){ return data.toString(); }
	
}//end queue
