package util.structures;



public class ListNode<E> {

	private E item;
	private ListNode<E> next;
	
	/**
	 * Constructor: creates a new list node
	 * @param i Object to store
	 * @param n Next node in the list
	 */
	public ListNode(E i, ListNode<E> n){
		item=i;
		next=n;
	}
	/**
	 * Constructor: Creates an new List Node, next = null
	 * @param i Object to store
	 */
	public ListNode(E i){
		item=i;
		next=null;
	}
	
	public void setNext(ListNode<E> n){next=n;}
	public void setItem(E i){item=i;}
	
	public E getItem(){return item;}
	public ListNode<E> getNext(){return next;}
	
	public String toString(){
		return item.toString();
	}
	
}//end ListNode
