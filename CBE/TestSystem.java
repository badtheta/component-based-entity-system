import java.util.ArrayList;

import Core.*;
import Core.System;
import Core.Annotations.ClassMap;
import Core.Managers.ComponentManager;
import Core.Managers.EntityManager;

import static java.lang.System.out;

public class TestSystem extends System{

	@ClassMap ComponentClassMapper<TestComponent> ts;
	
	public TestSystem(Realm rel, boolean b){
		super(b,rel);
		Requirement r = new Requirement(rel);
		r.all(TestComponent.class);
		this.setRequirements(r);
		ts = new ComponentClassMapper(TestComponent.class , this.place);
	}
	
	
	@Override
	public void effect() {
		out.println("****************Begin test system effect.***********");
		ArrayList<Entity> ents = place.getEntMan().getEntities(requirements);//get
		for(Entity e: ents){
			out.println("Entity    "+e);
			TestComponent[] tc = ts.getFromEntity(e);
			for(TestComponent t: tc){
				out.print("Update "+t.getUpdate()+"\t");
				t.incre();
				out.print("Update "+t.getUpdate()+"\n");
			}
		}
	}

}


